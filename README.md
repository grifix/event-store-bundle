Integration [grifix/event-store](https://packagist.org/packages/grifix/event-store) with [Symfony](https://symfony.com/)

# Installation
`composer require grifix/event-store-bundle`

# Usage
- Read the [Grifix Event Store documentation](https://gitlab.com/grifix/event-store/-/blob/2.0/README.md)
- set up configuration as in this example:
```yaml
    #set up database connection
    doctrine: 
      dbal:
        dbname: 'db'
        host: db
        port: 5432
        user: 'user'
        password: 'password'
        driver: pdo_pgsql
    
    grifix_event_store:
      #set up message_broker
      message_broker: 
        host: rabbitmq
        port: 5672
        user: 'user'
        password: 'password'
    
      #regiser streams
      streams: 
        - name: user
          producer_class: Grifix\EventStoreBundle\Tests\Dummies\User
    
      #regiser events 
      events: 
        #user.created
        - name: created
          stream: user
          event_class: Grifix\EventStoreBundle\Tests\Dummies\UserCreatedEvent
          version_converter: Grifix\EventStoreBundle\Tests\Dummies\UserVersionConverter
          schemas:
            #v1
            - - property: userId
                type: string
              - property: name
                type: string
            #v2
            - - property: userId
                type: string
              - property: name
                type: string
              - property: email
                type: string
        #user.deleted
        - name: deleted
          stream: user
          event_class: Grifix\EventStoreBundle\Tests\Dummies\UserDeletedEvent
          schemas:
            #v1
            - - property: userId
                type: string
              - property: name
                type: string
    
      #register subscriptions
      subscriptions:
        - name: user_subscription
          stream: user
          subscriber_class: Grifix\EventStoreBundle\Tests\Dummies\UserSubscriber
          starting_events: [user.created]
          finishing_events: [user.deleted]
    
    services:
      #register version converter
      Grifix\EventStoreBundle\Tests\Dummies\UserVersionConverter:
        public: true
    
      #register subscribers
      Grifix\EventStoreBundle\Tests\Dummies\UserSubscriber:
        public: true
```
- start the event publisher process by executing `grifix:event-store:run-event-publisher-worker` console command
- start the event consumer process by executing `grifix:event-store:run-event-consumer` console command  
- Inject event store as a dependency or get it from
    the [Symfony Container](https://symfony.com/doc/current/service_container.html)
    instead of creating it by `EventStore::create()`

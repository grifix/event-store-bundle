<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\DependencyProvider\DependencyProviderInterface;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\EventStore as BaseEventStore;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\StreamType\Repository\StreamTypeRepositoryInterface;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepositoryInterface;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;

final class EventStore extends BaseEventStore
{
    public function __construct(
        StreamTypeRepositoryInterface $streamTypeRepository,
        EventTypeRepositoryInterface $eventTypeRepository,
        SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        EventRepositoryInterface $eventRepository,
        SubscriptionRepositoryInterface $subscriptionRepository,
        EventPublisher $eventPublisher,
        EventProcessor $eventProcessor,
        DependencyProviderInterface $dependencyProvider,
        array $streamConfigs,
        array $subscriptionConfigs
    ) {
        parent::__construct(
            $streamTypeRepository,
            $eventTypeRepository,
            $subscriptionTypeRepository,
            $eventRepository,
            $subscriptionRepository,
            $eventPublisher,
            $eventProcessor,
            $dependencyProvider
        );

        $this->applyStreamConfigs($streamConfigs);
        $this->applySubscriptionConfigs($subscriptionConfigs);
    }

    private function applyStreamConfigs(array $streams): void
    {
        foreach ($streams as $stream) {
            $this->registerStreamType($stream['name'], $stream['producer_class'] ?? null);
            foreach($stream['events'] as $event){
                $this->registerEventType(
                    $stream['name'],
                    $event['name'],
                    $event['event_class'],
                    $this->createSchemas($event['schemas']),
                    $event['version_converter'] ?? null
                );
            }
        }
    }

    private function applySubscriptionConfigs(array $subscriptions)
    {
        foreach ($subscriptions as $subscribtion) {
            $this->registerSubscriptionType(
                $subscribtion['name'],
                $subscribtion['subscriber_class'],
                $subscribtion['stream'],
                $subscribtion['starting_events'],
                $subscribtion['finishing_events'] ?? [],
            );
        }
    }

    /**
     *
     * @return Schema[]
     */
    private function createSchemas(array $schemaConfigs): array
    {
        $result = [];
        foreach ($schemaConfigs as $schemaConfig) {
            $schema = Schema::create();
            $this->addPropertyToSchema($schema, $schemaConfig);
            $result[] = $schema;
        }
        return $result;
    }

    private function addPropertyToSchema(Schema $schema, array $schemaConfigs): void
    {
        foreach ($schemaConfigs as $propertyConfig){
            match ($propertyConfig['type']) {
                'object' => $schema->withObjectProperty(
                    $propertyConfig['property'],
                    $propertyConfig['allowed_normalizers'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'array_of_objects' => $schema->withArrayOfObjectsProperty(
                    $propertyConfig['property'],
                    $propertyConfig['allowed_normalizers'],
                ),
                'string' => $schema->withStringProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'integer' => $schema->withIntegerProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'number' => $schema->withNumberProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'array' => $schema->withArrayProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'boolean' => $schema->withBooleanProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                ),
                'mixed_object' => $schema->withMixedObjectProperty(
                    $propertyConfig['property'],
                    (bool)($propertyConfig['nullable'] ?? false)
                )
            };
        }
    }
}

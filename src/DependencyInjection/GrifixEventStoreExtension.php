<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\DependencyInjection;

use Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle;
use Grifix\ClockBundle\GrifixClockBundle;
use Grifix\EventStore\EventStoreInterface;
use Grifix\MemoryBundle\GrifixMemoryBundle;
use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Grifix\WorkerBundle\GrifixWorkerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

final class GrifixEventStoreExtension extends Extension implements PrependExtensionInterface
{

    public function prepend(ContainerBuilder $container)
    {
        $this->checkBundles($container);
        $this->setParameters($container);
        $this->loadConfigs($container);
    }

    private function checkBundles(ContainerBuilder $container): void
    {
        $bundles = $container->getParameter('kernel.bundles');
        $this->assertBundleEnabled(FrameworkBundle::class, $bundles);
        $this->assertBundleEnabled(GrifixNormalizerBundle::class, $bundles);
        $this->assertBundleEnabled(DoctrineMigrationsBundle::class, $bundles);
        $this->assertBundleEnabled(GrifixMemoryBundle::class, $bundles);
        $this->assertBundleEnabled(GrifixClockBundle::class, $bundles);
        $this->assertBundleEnabled(GrifixWorkerBundle::class, $bundles);
    }

    private function assertBundleEnabled(string $bundle, array $bundles)
    {
        if (!in_array($bundle, $bundles)) {
            throw new RuntimeException(sprintf('%s bundle must be enabled!', $bundle));
        }
    }

    private function loadConfigs(ContainerBuilder $container): void
    {
        $configDir = __DIR__ . '/../..';
        $loader = new YamlFileLoader($container, new FileLocator($configDir));
        $loader->load('config.yaml');
    }

    private function setParameters(ContainerBuilder $container): void
    {
        $container->setParameter(
            'grifix.event_store.path',
            dirname(
                (new \ReflectionClass(EventStoreInterface::class))->getFileName(),
                2
            )
        );
    }

    private function loadBundleConfig(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        foreach ($config as $key => $value) {
            $container->setParameter('grifix_event_store.' . $key, $value);
        }
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $this->loadBundleConfig($configs, $container);
    }
}

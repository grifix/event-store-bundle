<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_event_store');

        // @formatter:off
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('message_broker')
                    ->children()
                        ->scalarNode('host')->end()
                        ->integerNode('port')->end()
                        ->scalarNode('user')->end()
                        ->scalarNode('password')->end()
                    ->end()
                ->end()
                ->arrayNode('streams')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('producer_class')->end()
                            ->arrayNode('events')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('name')->end()
                                        ->scalarNode('event_class')->end()
                                        ->scalarNode('version_converter')->end()
                                        ->arrayNode('schemas')->cannotBeEmpty()
                                            ->arrayPrototype()
                                                ->arrayPrototype()
                                                    ->children()
                                                        ->scalarNode('property')->cannotBeEmpty()->end()
                                                        ->scalarNode('type')->cannotBeEmpty()->end()
                                                        ->arrayNode('allowed_normalizers')->cannotBeEmpty()
                                                            ->scalarPrototype()->end()
                                                        ->end()
                                                        ->scalarNode('nullable')->end()
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('subscriptions')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('stream')->end()
                            ->scalarNode('subscriber_class')->end()
                            ->arrayNode('starting_events')->scalarPrototype()->end()->end()
                            ->arrayNode('finishing_events')->scalarPrototype()->end()->end()
                        ->end()
                    ->end()
                ->end()
        ;
        // @formatter:on

        return $treeBuilder;
    }
}

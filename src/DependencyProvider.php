<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\DependencyProvider\DependencyProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class DependencyProvider implements DependencyProviderInterface
{
    public function __construct(private readonly ContainerInterface $container)
    {
    }

    public function set(object $dependency): void
    {
        throw new \Exception('Not implemented!');
    }

    public function get(string $className): object
    {
        return $this->container->get($className);
    }
}

<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\Tests;

use Doctrine\DBAL\Connection;
use Grifix\EventStore\Cli\PauseSubscriptionCommand;
use Grifix\EventStore\Cli\RunEventConsumerCommand;
use Grifix\EventStore\Cli\RunEventPublisherWorkerCommand;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventStore;
use Grifix\EventStore\EventStoreInterface;
use Grifix\EventStoreBundle\Tests\Dummies\User;
use Grifix\EventStoreBundle\Tests\Dummies\UserCreatedEvent;
use Grifix\Uuid\Uuid;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class EventStoreBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        TestHelper::setUpKernel($kernel);
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testInitBundle(): void
    {
        $container = self::getContainer();

        self::assertTrue($container->has(EventStoreInterface::class));
        $eventStore = $container->get(EventStoreInterface::class);
        $this->assertInstanceOf(EventStore::class, $eventStore);
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);
        TestHelper::setData(null);
        self::assertTrue($application->has(RunEventConsumerCommand::NAME));
        self::assertTrue($application->has(RunEventPublisherWorkerCommand::NAME));
        self::assertTrue($application->has(PauseSubscriptionCommand::NAME));
        TestHelper::executeCommand(
            $application,
            [
                'doctrine:migrations:migrate',
                '--no-interaction'
            ]
        );
        /** @var Connection $connection */
        $connection = $kernel->getContainer()->get('doctrine')->getConnection();
        $connection->executeQuery('truncate table grifix_event_store.events');
        $connection->executeQuery('truncate table grifix_event_store.subscriptions');
        $connection->insert('grifix_event_store.events', [
            'id' => 'b0d3c986-73f1-4a71-8b93-923141cc96e1',
            'stream_id' => '588a501c-5997-482d-8036-83faf16ff489',
            'stream_type_name' => 'user',
            'event_type_name' => 'created',
            'number' => 1,
            'date_of_creation' => '2022-07-13 06:12:54.782464',
            'event' => json_encode([
                'userId' => '588a501c-5997-482d-8036-83faf16ff489',
                'name' => 'Joe',
                '__normalizer__' => [
                    'name' => 'user.created',
                    'version' => 1
                ],
            ])
        ]);

        $eventStore = $kernel->getContainer()->get(EventStoreInterface::class);
        $event = new UserCreatedEvent(
            Uuid::createRandom()->toString(),
            'Mike',
            'mike@email.com'
        );
        $eventStore->storeEvent(
            $event,
            User::class,
            Uuid::createFromString($event->userId)
        );
        $connection->beginTransaction();
        $eventStore->flush();
        $connection->commit();
        /** @var EventEnvelope[] $events */
        $events = array(...$eventStore->findEvents());
        self::assertEquals(
            new UserCreatedEvent(
                '588a501c-5997-482d-8036-83faf16ff489',
                'Joe',
                'unknown'
            ),
            $events[0]->event
        );
        self::assertEquals(
            $event,
            $events[1]->event
        );
        sleep(1);
        self::assertEquals($event, TestHelper::getData());
    }
}

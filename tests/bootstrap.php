<?php
declare(strict_types=1);

use Symfony\Component\Dotenv\Dotenv;
$rootDir = dirname(__DIR__);
require $rootDir . '/vendor/autoload.php';
$dotenv = new Dotenv();
$dotenv->load($rootDir.'/.env');

<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\Tests\Dummies;

use Grifix\EventStoreBundle\Tests\TestHelper;

final class UserSubscriber
{
    public function onUserCreated(UserCreatedEvent $event)
    {
        TestHelper::setData($event);
    }
}

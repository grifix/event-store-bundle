<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\Tests\Dummies;

final class UserDeletedEvent
{
    public function __construct(
        public readonly string $userId
    ) {
    }
}

<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\Tests\Dummies;

final class UserCreatedEvent
{

    public function __construct(
        public readonly string $userId,
        public readonly string $name,
        public readonly string $email
    ) {
    }
}

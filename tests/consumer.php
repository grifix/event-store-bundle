<?php

declare(strict_types=1);

use Grifix\EventStoreBundle\Tests\TestHelper;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Dotenv\Dotenv;

$rootDir = dirname(__DIR__);
require $rootDir . '/vendor/autoload.php';
$dotenv = new Dotenv();
$dotenv->load($rootDir . '/.env');

$kernel = new TestKernel('test', true);
TestHelper::setUpKernel($kernel);

$kernel->boot();
$application = new Application($kernel);
$application->setAutoExit(false);
TestHelper::executeCommand($application, [
    'doctrine:migrations:migrate',
    '--no-interaction'
]);
TestHelper::executeCommand($application, [
    'grifix:event-store:run-event-consumer'
]);


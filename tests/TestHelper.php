<?php

declare(strict_types=1);

namespace Grifix\EventStoreBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle;
use Grifix\ClockBundle\GrifixClockBundle;
use Grifix\EventStoreBundle\GrifixEventStoreBundle;
use Grifix\MemoryBundle\GrifixMemoryBundle;
use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Grifix\WorkerBundle\GrifixWorkerBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

abstract class TestHelper
{

    private static function getPath(): string
    {
        return __DIR__ . '/test.dump';
    }

    public static function getData(): mixed
    {
        if (!file_exists(self::getPath())) {
            return null;
        }

        return unserialize(file_get_contents(self::getPath()));
    }

    public static function setData(mixed $value): void
    {
        file_put_contents(self::getPath(), serialize($value));
    }

    public static function setUpKernel(TestKernel $kernel):void{
        $kernel->addTestBundle(GrifixMemoryBundle::class);
        $kernel->addTestBundle(GrifixNormalizerBundle::class);
        $kernel->addTestBundle(GrifixClockBundle::class);
        $kernel->addTestBundle(GrifixWorkerBundle::class);
        $kernel->addTestBundle(DoctrineBundle::class);
        $kernel->addTestBundle(DoctrineMigrationsBundle::class);
        $kernel->addTestBundle(GrifixEventStoreBundle::class);
        $kernel->addTestConfig(__DIR__ . '/config.yaml');
    }

    public static function executeCommand(Application $application, array $params):void{
        $input = new ArrayInput($params);
        $output = new BufferedOutput();
        $application->run($input, $output);
    }
}
